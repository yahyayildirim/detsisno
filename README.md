# HAKKINDA
Bu Script, www.kaysis.gov.tr adresi üzerinden arama yaparak Kurumlara ait DETSİS NO ve diğer paylaşılan bilgileri getirmeye yarayan basit ama işlevsel bir scripttir. Kişisel Kullanım için hazırladım ancak herkesin istifadesine sunuyorum. Sizde dilediğiniz gibi kullabilirsiniz. Eksik ve yanlış yazdığım kodlar hakkında dönüş yaparsanız memnun olurum.

# KURULUM ve KULLANIM:
*1-* Bu programı kullanabilmeniz için zenity, curl ve w3m programları sisteminizde kurulu olması gerekiyor.
`sudo apt install zenity curl w3m` 

*2-* `wget -q -O /home/$USER/.local/bin/detsisno https://kod.pardus.org.tr/yahyayildirim/detsisno_bul/raw/master/detsisno` komutu ile dosyayı /home/$USER/.local/bin/ dizinine indiriyoruz.

*3-* `chmod +x /home/$USER/.local/bin/detsisno` komutu ile çalıştırılabilir iznini veriyoruz.

*4-*  `wget -q -O /home/$USER/.local/share/applications/detsisno.desktop https://kod.pardus.org.tr/yahyayildirim/detsisno_bul/raw/master/detsisno.desktop` komutu ile dosyayı indirip, Programlar menüsünde görünebilmesi için Uygulamalar dizinine atıyoruz.

*5-* Uygulamalar menüsünden DETSİS NO BUL'a tıklayarak arama yapmaya başlayabilirsiniz. Uçbirimde detsisno yazarakta scripti kullanabilirsiniz.